#!/usr/bin/env bash
#
# Archiver
#

# Parameters
BASENAME="`basename $0 | sed -e 's/\(.\)/\U\1/'`"
DIRNAME="`dirname $0`"
BASEDIR="$DIRNAME/.."
#ARCHIVE="$BASEDIR/archive"
ARCHIVE="archive"
ASSETS="build"
REVISION="`git describe --tags 2> /dev/null || git log -1 --format=oneline | cut -d ' ' -f 1`"

# Determine the revision file
if [ -e "$ASSETS/revision" ]; then
  #REVFILE="$BASEDIR/$ASSETS/revision"
  REVFILE="$ASSETS/revision"
else
  REVFILE="$ASSETS/book/revision"
fi

# Make sure the archive folder exist
mkdir -p $ARCHIVE

# Make sure the archive has some basic links
# This allows preservation of symbolic links for each archive version.
(
  cd $ARCHIVE &> /dev/null

  rm -f archive && ln -s . archive
  for item in slides vendor images LICENSE; do
    ln -sf ../$item
  done
)

# Check for previous build
if [ ! -e "$REVFILE" ]; then
  echo "# $BASENAME: skipping: revision file not found"
  exit
else
  OLD_REVISION="`cat $REVFILE`"
fi

# Check for non-null revision
if [ -z "$OLD_REVISION" ]; then
  echo "# $BASENAME: skipping: old revision is null"
  exit
fi

# Check if it's a tag
if git tag | grep -q "^${OLD_REVISION}"; then
  # Check if archive does not exists
  if [ ! -d "$ARCHIVE/$OLD_REVISION" ]; then
    echo "# $BASENAME: archiving $OLD_REVISION..."
    cp -alf $ASSETS $ARCHIVE/$OLD_REVISION
  else
    echo "# $BASENAME: revision $OLD_REVISION already archived"
  fi
else
  echo "# $BASENAME: not archiving: revision $OLD_REVISION is not a tag"
fi
