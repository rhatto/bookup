# ChangeLog

## v0.2.5 - 2025-01-18

* [x] Fix lookup for `js` folder for notes when the book is built under
      `build/book` (then search won't work because `build/book/js` won't be
      available).

## v0.2.4 - 2024-10-06

* [x] Apply the previous `NULL` fix to the portuguese template.

## v0.2.3 - 2024-10-06

* [x] Fix a `NULL` at the end of TOC in the PDF version.
      It was inserted at the TeX source, due to an empty `R` block.
* [x] Removes the unused `biblio-style` config.

## v0.2.2 - 2024-09-29

* [x] Display label before the book URL.

* [x] Fix publisher metadata information on the PDF output.
      This required changing changing the "publisher"
      metadata from `_common.yml` to `snippets/publisher.txt`.

## v0.2.1 - 2024-09-16

* [x] Fix TOC spacing between section numbers and section titles when there are
      many sections making the section number too big.

## v0.2.0 - 2024-08-16

* [x] Provision: ensure a recent ggplot2 is used.

## v0.1.9 - 2024-07-13

* [x] Fix link to the Bookup site in the documentation.

## v0.1.8 - 2024-07-13

* [x] Template translation fix.
* [x] Put list of etimologies before the list of definitions,
      as they may usually come first (at least historically).

## v0.1.7 - 2024-06-29

* [x] Do not include the references block at the end of every file processed.

## v0.1.6 - 2024-06-19

* [x] Apply fixes to the archive/permalink logic.

## v0.1.5 - 2024-06-19

* [x] Archive after doing a build, making sure that the current tag is already
      in the archive (and hence can have a permalink upfront).

## v0.1.4 - 2024-06-11

### Fixes

* [x] Make sure the archive folder has some basic
  links.

### Features

* [x] Fine-grained control of lists of tables,
      figures etc.
* [x] Scaffolding to support a "Recent changes"
      section in the compiled book.
* [x] Documentation: Bookup features, and minor
      fixes.

### Misc

* [~] Move `slides` to `content/slides`?

## v0.1.3 - 2024-06-10

### Fixes

* [x] Fix bibliography handling.

## v0.1.2 - 2024-06-10

### Features

* [x] Improved BibTeX bibliography handling.

## v0.1.1 - 2024-06-10

### Fixes

* [x] GitLab pages.

## v0.1.0 - 2024-06-10

### Fixes

* [x] Cover page dimensions.

## v0.0.9 - 2024-06-10

### Fixes

* [x] GitLab pages.

## v0.0.8 - 2024-06-10

### Features

* [x] Localization support:
  * [x] Detect the `lang` YAML parameters and use templates from
        `{templates,structure}/{book,notes}/$lang`.
  * [x] Compile the `_output.yml` from a template, setting the `includes`
        from the appropriate language-based structure files.
  * [x] Translate documentation to English.

## v0.0.7 - 2024-06-09

### Features

* [x] GitLab CI.

## v0.0.6 - 2024-06-09

### Features

* [x] Notebook:
  * [x] Add link to the notebook in the main book HTML.
  * [x] Add license and version information in the notebook.
* [x] Add the repository URL into the
  documentation.
* [x] Improvements towards localization.

### Issues

* [x] Add a `test` folder for quick experiments.

## v0.0.5 - 2024-06-09

### Features

* [x] Improved compilation procedures.
* [x] Unified configuration for book and notes.

## v0.0.4 - 2024-06-08

### Fixes

* [x] Use `book` instead of `compiled`.
* [x] Use `build` instead of `public`.
* [x] Use `content/sections` instead of `content/published`.
* [x] List of definitions and etimologies breaks build if they're empty
      (`99-end.tex`).

### Misc

* [~] Tests for quickly trying things? No need, simply try on this repository
      directly.

## v0.0.3 - 2024-06-08

### Features

* [x] Notebook support.
* [x] Website support.

## v0.0.2 - 2024-06-06

### Features

* [x] Improved documentation.

## v0.0.1 - 2024-06-06

### Features

* [x] Package a Bookdown-based publishing solution in a project named `bookup`
      ("a bookdown-based publishing solution")
      or similar, to ease book editing and authorship.
* [x] Use the repository [ensaios][] as base.
* [x] Create a basic documentation.

[ensaios]: https://ensaios.fluxo.info
