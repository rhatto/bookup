# Introduction {#intro}

Bookup^[@bookup] is a set of templates, scripts and workflows for book
authoring in HTML, PDF and other formats.

It's a modular software component based on [Bookdown][].

It was created to solve the problem of editing many books simultaneously:
Bookup helps to keep an uniform scheme instead of having to implement and fix
funcionalities across many repositories in parallel.

[Bookup][] makes book production similar to working with other codebases.

But [Bookup][] is very opinionated.
You can understand it as an specific setup of [Bookdown][] and other software,
plus conventions and heuristics tuned to an specific use case.

[Bookup][] code is available at [https://0xacab.org/rhatto/bookup][].

[Bookdown]: https://bookdown.org
[Bookup]: https://bookup.fluxo.info
[https://0xacab.org/rhatto/bookup]: https://0xacab.org/rhatto/bookup
