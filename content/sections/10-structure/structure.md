# Folder structure {#structure}

A Bookup project folder structure looks like this:

* `archive`: stores old book versions.
* `content`: the actual content:
  * `drafts`: you can leave here texts still being composed.
  * `sections`: here goes the material to be compiled in book format.
  * `notes`: keep texts here to be compiled as a notebook companion.
  * `slides`: keep the slides sources here.
* `site`: an optional website used as base; if that exists, the book will be
          place inside the `build/book` folder.
* `slides`: if this folder is available, it will be considered as the compiled
   slides folder and will be included in the `build/slides` folder.
* `build`: the compiled book (HTML, PDF and other formats):
  * `notes`: the compiled notebook.
  * `book`: the compiled book, if there's a `site` folder available,
    otherwise the book content will be placed in the toplevel `build` folder.
* `biblio`: bibliography (BibTeX).
* `images`: image folder.
* `vendor/bookup`: where the Bookup codebase actually resides:
  * `content`: Bookup documentation.
  * `vendor/{Fuse.js,}`: dependencies.
