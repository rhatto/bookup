# Installation {#installation}

Bookup installation procedure.

## Vendorization

* Vendorization (`bookup` and bibliography repositories).

## Symbolic links

* `_bookdown.yml`.

## Required files

* `.gitignore`.
* `_common.yml`.
* `_book.yml`.
* `_notes.yml`.
* `_biblio.yml`.
* `_output.yml`.
* `Makefile`.
* `LICENSE`.
* `DISCLAIMER`.
* `images/cover.png`.
* `snippets/{contact,cover,keywords,project,url,volume}.txt`.
* `snippets/changes.md`.

## Optional files

* `snippets/header.tex`.
