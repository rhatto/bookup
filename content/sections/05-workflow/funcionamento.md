# Workflow {#workflow}

Bookup works through _compiling_ files from source:

```
                       \                         .--> PDF book
Layout (LaTeX and HTML) \                       /
Source text (Markdown)   >---> Compilation ----<
Bibliography (BibTeX)   /                       \
                       /                         `--> HTML book
```
