# Features {#features}

Main Bookup features:

* Makes a book website (HTML format) already integrated with the printer and
  ebook-friendly version (PDF): the PDF is linked and embeded in the website.

* Builds a companion HTML notebook that can be shared along with the main book,
  allowing authors to keep additional notes, which is especially handy during
  book composition.

* Automatically archives old book versions based on Git tags during the build
  process.

* Automatic bibliography management with BibTeX: all `.bib` files recursivelly
  found in the project folder are made available during compilation.

* Adds the compilation metadata:
  * Version based on Git tag or commit.
  * Compilation date.

* Localization support: templates can be easily translated to other languages.
