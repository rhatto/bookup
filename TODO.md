# TODO

## Issues

* [ ] Bug in `pandoc` with `citeproc` ignoring citation inside LaTeX blocks.
      Affecting the PDF format. Test available at `test/pandoc`.
      Consider to test with the latest pandoc and citeproc versions.
      Consider an upstream bug report.
      If needed, also apply corrections in the existing downstream projects.
* [ ] Support to be hosted under relative URLs, perhaps using the HTML
      `<base>` tag.
* [ ] Upstream on `tufte-latex` the fix for `\titlecontents{section}` when
      there are many sections within a chapter: increase, or make customizable,
      the width between the section number and the section title.
* [ ] Bibliography should honor the `urldate` BibTeX field.

## Refactor

* [ ] Create a `bookup` script:
  * [ ] Able to read all the YAML config files, which should be the central
        places to hold all the configuration.
  * [ ] With templating support to customize the existing templates.
        This may make the `snippets` files to be compiled during build time.
  * [ ] Which then dispatch compilation etc to the other scripts.
  * [ ] Merge `structure` and `templates` folders?
  * [ ] Detect the `slides` folder and add a link to it in the compiled docs.

## Improvements

* [ ] Archive:
  * [ ] Data deduplication (and compression of old archives), to save space.
* [ ] Slides:
  * [ ] Improved slides workflow (compilation etc).
* [ ] Citation:
  * [ ] How to cite, by using `@projectName` in the frontmatter.
  * [ ] Add the corresponding BibTeX entry.
* [ ] Recent changes:
  * [ ] Build the recent changes section through a `snippets/changes.md` file?
      This is partially implemented, but commented.
      As an alternative (or meanwhile), the notebook can be used.
  * [ ] Alternativelly, build the recent changes using a `snippets/changes.yml`
        or just `_changes.yml` that gets compilet into a `snippets/changes.md`
        and into a RSS file that goes in the build.
        This RSS them can be linked as a `rel` element in the HTML, along with
        a regular link like "Follow updates in this book".
* [ ] Localization:
  * [ ] Make sure to properly localized the build date string.
  * [ ] Setup a PO-file workflow for translating the structure and templates?
* [ ] Formats:
  * [ ] Fix the EPUB output:
        https://bookdown.org/yihui/bookdown/e-books.html
        https://github.com/rstudio/bookdown/issues/1179
        https://github.com/rstudio/bookdown/issues/1387
        https://github.com/rstudio/bookdown/blob/main/NEWS.md#major-changes
  * [ ] ZIM format using the `zim-tools` package for the book and the notes.
  * [ ] Update the current available output formats.
* [ ] Backmatter:
  * [ ] Customized lists of definitions, hypothesis, etimologies, examples etc
        in the PDF version:
        https://tex.stackexchange.com/questions/180747/editing-format-of-list-of-theorems-in-thmtools
        https://tex.stackexchange.com/questions/16494/generating-lists-of-custom-environment
  * [ ] Lists of definitions, hypothesis, etimologies, examples etc in the HTML version.
        Possible implementations:
        https://stackoverflow.com/questions/40735727/create-index-of-definitions-theorems-at-end-of-bookdown-book?rq=4
  * [ ] Backcover.
  * [ ] Book flaps (and/or dust jacket).
* [ ] Misc:
  * [ ] Spell checking as a `Makefile` target.
  * [ ] Suport for a `BASE_URL` environment variable?
        It would be applied as a template variable in `%%base_url%%` placeholers,
        allowing the compile material to have links among themselves.
  * [ ] Optionally add a small reference to the Bookup website in the book
        frontmatter?

## Documentation

* [ ] Improve the documentation.
* [ ] Document the "Bookup Stack": which piece of software is at which level,
      and used for what.
* [ ] Add a reference to:
  * [ ] Books using Bookup.
  * [ ] Similar projects, like:
    * [ ] [Repositórios da Editora Hedra](https://github.com/hedra-editora).
