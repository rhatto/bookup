#
# Global Makefile - https://templater.fluxo.info
#
# This Makefile contains basic, common targets and also includes
# any Makefile.* available in the current folder.
#

# Set CONTAINER based in what we have available in the system
# This variable can be user in other, included Makefiles to handle virtualization tasks
ifeq ($(shell which kvmx > /dev/null && test -s kvmxfile && echo yes), yes)
	CONTAINER = kvmx
else ifeq ($(shell which vagrant > /dev/null && test -s Vagrantfile && echo yes), yes)
	CONTAINER = vagrant
else ifeq ($(shell which docker > /dev/null && test -s Dockerfile && echo yes), yes)
	CONTAINER = docker
else
  CONTAINER = ''
endif

# Default action
default: all

# Process any other Makefile whose filename matches Makefile.*
# See https://www.gnu.org/software/make/manual/html_node/Include.html
#
# Some of those files might even contain local customizations/overrides
# that can be .gitignore'd, like a Makefile.local for example.
-include Makefile.*

# Customization examples can be as simple as setting variables:
#CONTAINER  = vagrant
#CONTAINER  = docker
#DESTDIR   ?= vendor
