#
# Parameters
#
# Customize to your needs in your main Makefile. Examples:
#
# 	BOOKUP  = vendor/bookup
# 	OUTPUT  = my-essay
# 	OUTPUT  = essay
# 	WEBSITE = some.hostname

# Bookup base folder
BOOKUP ?= .

# Project name
PROJECT ?= bookup

# Output file names (prefix)
OUTPUT ?= book

# Output folder
OUTPUT_FOLDER ?= $(OUTPUT)

# Destination website hostname (for rsync+ssh)
WEBSITE ?= $(PROJECT)

# HTML style
PDF_STYLE ?= bookdown::tufte_book2

# HTML style
#HTML_STYLE ?= bookdown::html_book
HTML_STYLE ?= bookdown::gitbook

#
# Exclusion patterns
#

#EXCLUDES = --exclude=.htpasswd
#EXCLUDES = --exclude=Makefile.local
EXCLUDES  = archive slides

#
# Runtime parameters
#

REVISION = $(shell git describe --tags 2> /dev/null || git log -1 --format=oneline | cut -d ' ' -f 1)
DATE     = $(shell date)
YEAR     = $(shell date +%Y)

#
# Phony targets
#

.PHONY: archive slides site

#
# Default target
#

all: submodules book notes site assemble clean archive
	@true

#
# Archive
#

archive:
	@$(BOOKUP)/bin/archive

#
# Cleanup
#

clean:
	@true
	@rm -f $(OUTPUT).aux $(OUTPUT).log $(OUTPUT).nav $(OUTPUT).out $(OUTPUT).snm $(OUTPUT).tex $(OUTPUT).toc $(OUTPUT).vrb
	@rm -f index.md index.Rmd
	@rm -f _main.log _main.tex _main.loe _main.Rmd
	@rm -rf _book _bookdown_files _main_files

#
# Submodules
#

submodules:
	@git submodule update --init --recursive

#
# Metadata
#

metadata:
	@mkdir -p           .metadata
	@echo $(REVISION) > .metadata/revision.txt
	@echo $(DATE)     > .metadata/date.txt
	@echo $(YEAR)     > .metadata/year.txt

#
# Compilation
#

compile_book: metadata
	@$(BOOKUP)/bin/compile-book $(OUTPUT) $(REVISION) $(DATE)

compile_notes: metadata
	@$(BOOKUP)/bin/compile-notes notes $(REVISION) $(DATE)

compile: compile_book move_book compile_notes move_note
	@true

#
# Main bookdown targets
#

bookdown_clean:
	@rm -f _main.Rmd

bookdown_html:
	# Cleanup
	@rm -rf $(OUTPUT_FOLDER)

	# Fix references
	# Use sed to fix a weird reference issue on fig.cap inside knitr blocks that are not properly converted.
	# Not using "sed -i" since it yields a permission denied error when running inside a guest VM.
	# This fix no longer works with citeproc.
	@#cp $(OUTPUT).md $(OUTPUT).md.base
	@#sed -e 's/\\\\citet{\([^}]*\)}/@\1/g' $(OUTPUT).md > $(OUTPUT).new && mv $(OUTPUT).new $(OUTPUT).md

	# Build
	Rscript -e 'bookdown::render_book("$(OUTPUT).md", "$(HTML_STYLE)")'

	# Restore $(OUTPUT).md after the weird reference fix
	@#mv $(OUTPUT).md.base $(OUTPUT).md

	# Fix the index file name
	# See https://stackoverflow.com/questions/58340924/bookdown-generates-index-file-with-a-chapter-title-instead-of-index-html-when
	# Fixed using section ID naming in the preamble
	#@cd _book && ln -s "introduction.html" index.html

	# Move the book to the output folder
	@mv _book $(OUTPUT_FOLDER)

	# Replace fuse.js from CDN to local
	# This ensures the HTML output can be browsed offline
	#@find $(OUTPUT)/html -name *.html -exec sed -i -e 's|https://cdn.jsdelivr.net/npm/fuse.js@[^"]*|../../vendor/Fuse.js/dist/fuse.min.js|' {} \;
	@mkdir $(OUTPUT_FOLDER)/js
	@cp vendor/bookup/vendor/Fuse.js/dist/fuse.min.js $(OUTPUT_FOLDER)/js
	@find $(OUTPUT_FOLDER) -name *.html -exec sed -i -e 's|https://cdn.jsdelivr.net/npm/fuse.js@[^"]*|js/fuse.min.js|' {} \;

  # Add revision information
	@echo $(REVISION) > $(OUTPUT_FOLDER)/revision

	# Add symlink to the archive and to the slides
	# This symlink is useful for checking things in localhost
	# But it tends to be replaced during remote deployment
	@cd $(OUTPUT_FOLDER) > /dev/null && ln -s ../archive
	@cd $(OUTPUT_FOLDER) > /dev/null && ln -s ../slides

bookdown_pdf:
	# Cleanup old builds
	@rm -rf _book/_main.pdf

	# Build
	@Rscript -e 'bookdown::render_book("$(OUTPUT).md", "$(PDF_STYLE)")'

	# Move
	@mv _book/_main.pdf $(OUTPUT_FOLDER)/$(OUTPUT).pdf

# Use sed to convert tufte::margin_note to marginfigure
# Also, "sed -i" yields a permission denied error when running inside a guest VM.
bookdown_epub:
	@# EPUB format is broken right now
	@true

	# Fix margin notes
	@#sed -e 's/r tufte::margin_note/marginfigure/' $(OUTPUT).md > $(OUTPUT).new && mv $(OUTPUT).new $(OUTPUT).md

	# Build
	@#Rscript -e 'bookdown::render_book("$(OUTPUT).md", "bookdown::epub_book")'

	# Move
	@##mv _book/_main.epub $(OUTPUT)/$(OUTPUT).epub
	@#mv _book/_main.epub $(OUTPUT_FOLDER)/$(OUTPUT)s.epub

	# Cleanup
	@#rm _book/reference-keys.txt
	@#rmdir _book

bookdown: bookdown_clean bookdown_html bookdown_pdf bookdown_epub

#
# Build the book
#

move_book:
	@mv $(OUTPUT).* $(OUTPUT_FOLDER)/

assemble:
	@$(BOOKUP)/bin/assemble

book: compile_book bookdown move_book clean

#
# Notes
#

move_notes:
	@mv notes.* notebook

# Too many section levels (nesting) to produce a PDF output
#bookdown_notes_pdf:
#	@Rscript -e 'bookdown::render_book("notes.Rmd", "$(PDF_STYLE)")'

bookdown_notes_html:
	# Fix references
	# Use sed to fix a weird reference issue
	# Also, "sed -i" yields a permission denied error when running inside a guest VM.
	@sed -e 's/\\\\citet{\([^}]*\)}/@\1/g' notes.md > notes.new && mv notes.new notes.md

	# Build
	@Rscript -e 'bookdown::render_book("notes.Rmd", "$(HTML_STYLE)")'

	# Cleanup old notes
	@rm -rf notebook

	# Move
	@mv _book notebook

	# Replace fuse.js from CDN to local
	# This ensures the HTML output can be browsed offline
	#
	# We could simply assume that there's a ../js/fuse.min.js, but that may not
	# be always the case.
	#
	# Instead, we ship it's own js/ folder, making notes self-contained.
	#
	# Another approach would be to have a common, parent js/ folder for both the
	# book and the notes, to minimize loading external resources.
	@#find notebook -name *.html -exec sed -i -e 's|https://cdn.jsdelivr.net/npm/fuse.js@[^"]*|../js/fuse.min.js|' {} \;
	@mkdir notebook/js
	@cp vendor/bookup/vendor/Fuse.js/dist/fuse.min.js notebook/js
	@find notebook -name *.html -exec sed -i -e 's|https://cdn.jsdelivr.net/npm/fuse.js@[^"]*|js/fuse.min.js|' {} \;

notes: compile_notes bookdown_clean bookdown_notes_html move_notes clean

#
# Website
# This is a phony target to be overrided in another Makefile
#
site:
	@true

#
# Remote copies
#

web_deploy:
	@rsync -avz --delete $(EXCLUDES) ./build/   $(WEBSITE):/
	@rsync -avz --delete             ./archive/ $(WEBSITE):/archive/
	@rsync -avz --delete             ./slides/  $(WEBSITE):/slides/

#
# Offline copies
#

usb_stick:
	sudo rsync -av --exclude=backups --delete ./ /media/usb/$(PROJECT)/
