<!-- Include the references subsection only in the HTML version -->
```{r echo=FALSE, results='asis'}
if (knitr::is_html_output()) {
  cat("## References")
  cat("\n")
  cat("\n")
}
```
