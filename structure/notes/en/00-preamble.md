<!-- Bookdown HTML before-body file -->

<!-- Hack to generate a proper cover and fix the missing index.html file -->
<!-- See https://stackoverflow.com/questions/58340924/bookdown-generates-index-file-with-a-chapter-title-instead-of-index-html-when -->
<!-- See https://bookdown.org/yihui/rmarkdown-cookbook/document-metadata.html -->
```{r echo=FALSE, results='asis'}
if (knitr::is_html_output()) {
  cat('# Start {#index .unnumbered}')
  cat("\n")
  cat('<img src="images/cover.png">')
  cat("\n")
  cat("<h2>About</h2>")
  cat("\n")
  cat('<em>')
  cat(readLines('DISCLAIMER'), sep='\n')
  cat('</em>')
  cat('<br/><br/>')
  cat('Living Notebook - Version <strong><em>')
  cat(readLines('.metadata/revision.txt'), sep='\n')
  cat('</em></strong> - compiled at <strong><em>')
  cat(readLines('.metadata/date.txt'), sep='\n')
  cat('</em></strong>.')
  cat('<br/><br/>')
  cat("\n")
  cat("<h2>Credits</h2>")
  cat("\n")
  cat(paste(rmarkdown::metadata$title, "<br/>", "Copyleft &copy;", readLines('.metadata/year.txt'), rmarkdown::metadata$author, readLines('snippets/contact.txt'), sep=' '))
  cat('<br/><br/>')
  cat(readLines('LICENSE'), sep='\n')
  cat('<br/><br/>')
}
```
