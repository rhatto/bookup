<!-- Bookdown before-body file -->

<!-- Hack to generate a proper cover and fix the missing index.html file -->
<!-- See https://stackoverflow.com/questions/58340924/bookdown-generates-index-file-with-a-chapter-title-instead-of-index-html-when -->
<!-- See https://bookdown.org/yihui/rmarkdown-cookbook/document-metadata.html -->
```{r echo=FALSE, results='asis'}
if (knitr::is_html_output()) {
  cat('# Start {#index .unnumbered}')
  cat("\n")
  cat('<img src="images/cover.png">')
  cat("\n")
  cat("<h2>About</h2>")
  cat("\n")
  cat('<em>')
  cat(readLines('DISCLAIMER'), sep='\n')
  cat('</em>')
  cat('<br/><br/>')
  cat('Living Book - Version <strong><em>')
  cat(readLines('.metadata/revision.txt'), sep='\n')
  cat('</em></strong> - compiled at <strong><em>')
  cat(readLines('.metadata/date.txt'), sep='\n')
  cat('</em></strong>.')
  cat('<br/>')
  cat('Older versions available in the <a href="/archive">archive</a>.')
  cat('<br/>')
  cat('Notebook available <a href="/notes">here</a>.')
  cat('<br/><br/>')
  cat(readLines('snippets/project.txt'))
  cat(' - ')
  cat(readLines('snippets/volume.txt'))
  cat('<br/>')
  cat('Published by ')
  #cat(rmarkdown::metadata$publisher)
  cat(readLines('snippets/publisher.txt'))
  cat('<br/>')
  cat('Published at ')
  cat(readLines('snippets/url.txt'), sep='\n')
  cat('<br/><br/>')
  cat(readLines('snippets/keywords.txt'), sep='\n')
  cat("\n")
  cat("<h2>Credits</h2>")
  cat("\n")
  cat(paste(rmarkdown::metadata$title, "<br/>", "Copyleft &copy;", readLines('.metadata/year.txt'), rmarkdown::metadata$author, readLines('snippets/contact.txt'), sep=' '))
  cat('<br/><br/>')
  cat(readLines('LICENSE'), sep='\n')
  cat('<br/><br/>')
  cat(readLines('snippets/cover.txt'), sep='\n')
  cat("<h2>PDF version</h2>")
  cat("\n")
  cat('Download the PDF <a href="book.pdf">here</a>, or browse it below:')
  cat("\n")
  cat('<br/><br/>')
  cat('<embed src="book.pdf" width="100%" height="375" type="application/pdf">')
  cat('<br/><br/>')
}
```

<!-- Sample block for the LaTeX output -->
```{r echo=FALSE, results='asis'}
if (knitr::is_latex_output()) {
  # By default, it outputs nothing.
  # An empty code block would return the string 'NULL' to LaTeX, which would then be printed.
  cat('')
}
```
