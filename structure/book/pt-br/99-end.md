\backmatter

<!-- Neat way to have HTML comments: https://stackoverflow.com/a/43902043 -->
<!-- This seems not to be the case anymore: https://bookdown.org/yihui/bookdown/citations.html -->
<script type="text/html">
`r if (knitr::is_html_output()) '# Bibliografia'`
</script>

# Bibliografia

<!-- Neat way to have HTML comments: https://stackoverflow.com/a/43902043 -->
<!-- This seems not to be the case anymore: https://bookdown.org/yihui/bookdown/citations.html -->
<script type="text/html">
```{r echo=FALSE, results='asis'}
if (knitr::is_html_output()) {
  cat(readLines('snippets/changes.md'), sep='\n')
}
```
</script>
