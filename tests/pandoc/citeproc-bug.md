---
  title: "Test"
  biblio-style: apalike
  link-citations: true
  references:
  - type: article-journal
    id: test
    author:
    - family: Doe
      given: J.
    issued:
      date-parts:
      - - 1900
        - 1
        - 1
    title: 'Test article'
    title-short: Test
    container-title: Nurture
    volume: 1
    issue: 1
    page: 1-10

---

# Pandoc issues with Citeproc

## About

This shows where citations are not properly converted by Pandoc/Citeproc.

## How to use

Compile with:

    pandoc -i citeproc-bug.md -t latex -o citeproc-bug.md --citeproc

## Tests

Normal: @test

Brackets syntax: [@test]

Brace syntax: @{test}

Inside braces: {@test}

Braces within braces: {@{test}}

Escaped braces: \{@test\}

Slash: \ @test

LaTeX block: \emph{@test}

LaTeX block 2: \emph{Reference to @test inside braces}

LaTeX block with escaped braces: \emph\{@test\}

LaTeX block with braces within braces: \emph{@{test}}

LaTeX protect block: \protect\emph{@test}

Preformatted:

  > @test
  >
  > \emph{@test}
