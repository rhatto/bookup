# Bookup

Bookup is an _opinionated_ book publishing template based on [Bookdown][].

It is specially useful for reusing the existing design an workflow across
multiple book volumes.

It was conceived as the underlying system used by [Projeto Vertigem][].

It has _lots_ of conventions and heuristics.

At this point Bookup is not highly customized, and probably will not suit
your needs, but still can be used as a reference if you're planning to do
something similar.

A sample content is available in the [Bookup website][].

[Bookdown]: https://bookdown.org
[Projeto Vertigem]: https://vertigem.fluxo.info
[Bookup website]: https://bookup.fluxo.info
