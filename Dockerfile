FROM debian:bookworm AS compiler
MAINTAINER Silvio Rhatto <rhatto@riseup.net>

COPY bin/provision   /usr/local/bin
COPY bin/provision.R /usr/local/bin

RUN apt-get update && \
    apt-get upgrade -y

RUN /usr/local/bin/provision

RUN apt-get install git -y

RUN rm -rf /var/lib/apt/lists/*

ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"
ARG APP="${APP:-compiler}"
ENV APP_BASE="${APP_BASE:-/srv/shared}"

RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown ${APP}. /home/${APP}

RUN mkdir -p ${APP_BASE}

RUN chown -R ${APP}.${APP} ${APP_BASE}

USER ${APP}

WORKDIR ${APP_BASE}

FROM debian:bookworm AS site
MAINTAINER Silvio Rhatto <rhatto@riseup.net>

RUN apt-get update && \
    apt-get upgrade -y

COPY bin/provision-host /usr/local/bin

RUN /usr/local/bin/provision-host

RUN sed -i -e 's/<VirtualHost 127.0.0.1:80>/<VirtualHost *:80>/' /etc/apache2/sites-available/onion.conf

FROM site AS tor

RUN sed -i -e 's/RunAsDaemon 1/RunAsDaemon 0/' /etc/tor/torrc
RUN sed -i -e 's/HiddenServicePort 80 127.0.0.1:80/HiddenServicePort 80 site:80/' /etc/tor/torrc

USER debian-tor
